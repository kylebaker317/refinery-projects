module Refinery
  module Projects
    class StylesController < ::ApplicationController

      before_action :find_all_styles
      before_action :find_page

      def index
        # you can use meta fields from your model instead (e.g. browser_title)
        # by swapping @page for @style in the line below:
        present(@page)
      end

      def show
        @style = Style.friendly.find(params[:id])
        @projects = @style.projects

        # you can use meta fields from your model instead (e.g. browser_title)
        # by swapping @page for @style in the line below:
        present(@page)
      end

    protected

      def find_all_styles
        @styles = Style.order('position ASC')
      end

      def find_page
        @page = ::Refinery::Page.where(link_url: "/styles").first
      end
      private


    end
  end
end