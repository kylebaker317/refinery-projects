Refinery::Core::Engine.routes.draw do

  # Admin routes
  namespace :projects, :path => '' do
    namespace :admin, :path => Refinery::Core.backend_route do
      resources :projects, :except => :show do
        collection do
          post :update_positions
          get 'delete_unused_images'
        end
      end
    end
    namespace :admin, :path => "#{Refinery::Core.backend_route}/projects" do
      resources :styles, :except => :show do
        collection do
          post :update_positions
        end
      end
    end
  end

# Frontend routes
  scope :styles, as: 'projects' do
    resources :styles, :path => '', module: 'projects', only: [:index, :show]
  end

  get "/styles/:style_id/:id(.:format)", :to => "projects/projects#show", :as => :projects_project

  namespace :projects do
    resources :projects, :path => '', :only => [:index]
  end
end