class AddSlugToRefineryProjectsStyles < ActiveRecord::Migration
  def change
    add_column :refinery_projects_styles, :slug, :string
    add_index :refinery_projects_styles, :slug
  end
end
