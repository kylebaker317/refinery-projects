class AddFeaturedProjectToRefineryProjectsProject < ActiveRecord::Migration
  def change
    add_column :refinery_projects, :featured, :boolean, default: false
  end
end
