# Open the Refinery::Admin::ImagesController controller for manipulation
require 'refinery/admin/images_controller.rb'
Refinery::Admin::ImagesController.class_eval do
#   def image_params_with_my_params
#     image_params_without_my_params.merge(params.require(:image).permit(:project_id, :image))
#   end
#
#   alias_method_chain :image_params, :my_params
  before_action :find_all_projects, except: [:show, :destroy]
  before_action :find_all_styles, only: [:new, :insert]
#
#   def image_params
#     params.require(:image).permit(permitted_image_params)
#   end
#
#   def self.nomes
#     puts "nomes"
#   end
  protected
  def find_all_projects
    @projects = Refinery::Projects::Project.all
  end

  def find_all_styles
    @styles = Refinery::Projects::Style.all
  end
#
#   private
#
#   def permitted_image_params
#     [
#         :image, :image_size, :image_title, :image_alt
#     ]
#   end
end
# https://gitter.im/refinery/refinerycms/archives/2015/07/08
module RefineryProjectIdImagesControllerDecorator
  def permitted_image_params
    super <<  [:project_id, :image]
  end
end

Refinery::Admin::ImagesController.send :prepend, RefineryProjectIdImagesControllerDecorator