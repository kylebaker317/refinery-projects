Refinery::Image.class_eval do
  # Add an association to the Refinery::Projects::Project class, so we
  # can take advantage of the magic that the class provides
  belongs_to :refinery_projects, :class_name => '::Refinery::Projects::Project'
end