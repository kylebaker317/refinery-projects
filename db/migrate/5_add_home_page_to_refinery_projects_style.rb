class AddHomePageToRefineryProjectsStyle < ActiveRecord::Migration
  def change
    change_table :refinery_projects_styles do |t|
      t.references :style, index: true
    end
  end
end