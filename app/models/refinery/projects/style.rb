module Refinery
  module Projects
    class Style < Refinery::Core::BaseModel
      extend FriendlyId
      friendly_id :name, use: [:slugged]
      after_create :create_style_link
      before_create :set_position
      after_update :update_style_link
      after_destroy :destroy_style_link
      validates :name, presence: true, uniqueness: true
      validate :only_on_in_home, if: :show_in_home_changed?

      has_many :projects
      has_many :subStyles, class_name: 'Style', foreign_key: 'style_id'
      belongs_to :style, class_name: 'Style'
      #has_one :main_project, class_name: 'Refinery::Projects::Project'

      # To enable admin searching, add acts_as_indexed on searchable fields, for example:
      #
      acts_as_indexed fields: [:name]

      def main_project
        if self.project_id.nil?
          nil
        else
          Refinery::Projects::Project.find(self.project_id)
        end
      end

      private
      def only_on_in_home
        number_of_styles_in_home = 0
        puts 'TEST'
        number_of_styles_in_home = 1 if self.show_in_home
        pages_on_home_page = Refinery::Projects::Style.where(show_in_home: true)
        error_string = ''
        if (number_of_styles_in_home + pages_on_home_page.count) >= 3
          pages_on_home_page.each_with_index do |home_style, index|
            if index == 0
              error_string += "'#{home_style.name}'"
            else
              error_string += " and '#{home_style.name}'."
            end
          end
          errors.add(:show_in_home,
                     "Error | Only 2 Styles can be in the home page, there are currently #{number_of_styles_in_home+1}, #{error_string}")
        end


      end

      def boolean_to_integer(boolean)
        if boolean
          1
        else
          0
        end
      end

      def set_position
        last_position = Refinery::Projects::Style.maximum(:position)
        self.position = last_position.to_i + 1
      end

      def destroy_style_link
        page_title = self.name
        destroyed_page = Refinery::Page.find_by title: page_title
        unless destroyed_page.nil?
          destroyed_page.delete
        end
      end

      def create_style_link
        if self.show_in_menu
          page_title = self.name
          parent_id = (Refinery::Page.find_by title: 'Styles').id
          link_url = "styles/#{self.slug}"
          Refinery::Page.create title: page_title, parent_id: parent_id, link_url: link_url, editable: false

        else
          destroy_style_link
        end
      end

      def update_style_link
        if self.show_in_menu
          old_name = old_attribute_value :name
          old_page = Refinery::Page.where(title: old_name).first
          if old_page.nil?
            create_style_link
          else
            old_page.update_attribute :title, self.name
          end
        else
          destroy_style_link
        end
      end
    end
  end
end