class AddMainProjectToRefineryProjectsStyles < ActiveRecord::Migration
  def change
    add_column :refinery_projects_styles, :project_id, :integer
  end
end
