class AddRefineryProjectsToRefineryTestimonials < ActiveRecord::Migration
  def change
    add_reference :refinery_clients_testimonials, :refinery_projects, index: true
    add_foreign_key :refinery_clients_testimonials, :refinery_projects, column: :refinery_projects_id
  end
end
