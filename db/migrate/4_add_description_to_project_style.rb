class AddDescriptionToProjectStyle < ActiveRecord::Migration
  def change
    add_column :refinery_projects_styles, :description, :text
  end
end
