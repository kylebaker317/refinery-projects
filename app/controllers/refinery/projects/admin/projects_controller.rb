module Refinery
  module Projects
    module Admin
      class ProjectsController < ::Refinery::AdminController
        before_action :find_all_project_styles, except: [:show, :destroy]
        before_action :find_all_images, except: [:show, :destroy]
        before_action :find_project_images, only: [:edit, :update]
        include Refinery::Admin::ImagesHelper
        crudify :'refinery/projects/project'

        def new
          @project = Project.new temp_id: rand(Time.now.yesterday.to_i * Time.now.to_i).to_s[-9..-1].to_i
          @project.build_address
          @project_images = []

        end

        def edit
          @project = Project.friendly.find(params[:id])
          @project.temp_id = rand(Time.now.yesterday.to_i * Time.now.to_i).to_s[-9..-1].to_i
          @projects = Refinery::Projects::Project.all
          @project_images = @project.refinery_images
          @styles = Refinery::Projects::Style.all
          @project.address || @project.build_address
        end

        def delete_unused_images
          # redirect_to projects_admin_projects_path
          if params[:project].length == 9
            image_to_delete = params[:project].to_i
            Refinery::Image.where(project_id: image_to_delete).destroy_all
          end
          render nothing: true
        end

        protected
        def find_all_project_styles
          @styles = Refinery::Projects::Style.all
        end

        def find_all_images
          @images = Refinery::Image.all
        end

        def find_project_images
          @project_images = @project.refinery_images
        end

        private

        # Only allow a trusted parameter "white list" through.
        def project_params
          params.require(:project).permit(:title,
                                          :date,
                                          :description,
                                          :style_id,
                                          :main_image_id,
                                          :featured,
                                          :position,
                                          :temp_id,
                                          refinery_images_attributes:
                                              [
                                                  :project_id,
                                                  :image_name,
                                                  :id,
                                                  :image,
                                                  :project_id,
                                                  :image,
                                                  :image_title,
                                                  :image_alt
                                              ],
                                          address_attributes:
                                              [
                                                  :id,
                                                  :line1,
                                                  :line2,
                                                  :city,
                                                  :state,
                                                  :zip
                                              ])
        end
      end
    end
  end
end
