module Refinery
  module Projects
    class Project < Refinery::Core::BaseModel
      extend FriendlyId
      friendly_id :title, use: [:slugged]
      self.table_name = 'refinery_projects'
      validates :title, presence: true, uniqueness: true
      validates :style_id, presence: true
      before_create :set_position
      after_create :add_images_to_project

      acts_as_indexed fields: [:title]
      attr_accessor :temp_id, :main_image
      has_many :refinery_images, foreign_key: 'project_id', class_name: '::Refinery::Image'
      accepts_nested_attributes_for :refinery_images
      belongs_to :style, class_name: '::Refinery::Projects::Style'
      belongs_to :refinery_clients, class_name: '::Refinery::Clients::Client'
      # alias_attribute :client, :refinery_clients
      has_many :testimonials, class_name: '::Refinery::Clients::Testimonial', foreign_key: 'refinery_projects_id'

      has_one :address, as: :addressable
      accepts_nested_attributes_for :address

      def main_image
        if self.main_image_id.nil?
          nil
        else
          Refinery::Image.find self.main_image_id
        end
      end

      protected
      def add_images_to_project
        Refinery::Image.where(project_id: self.temp_id).update_all(project_id: self.id)
      end

      private
      def set_position
        last_position = Refinery::Projects::Project.maximum(:position)
        self.position = last_position.to_i + 1
      end

    end
  end
end
