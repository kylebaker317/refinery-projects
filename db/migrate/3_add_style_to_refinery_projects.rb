class AddStyleToRefineryProjects < ActiveRecord::Migration
  def change
    add_column :refinery_projects, :style_id, :integer
  end
end
