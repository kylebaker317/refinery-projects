# Open the Refinery::PagesController controller for manipulation
Refinery::PagesController.class_eval do
  before_action :find_home_styles, except: [:show, :destroy]
  ADDRESS = "1644 Fry Rd | Greenwood, IN 46142"
  PHONE = "317.887.2778"
  protected
  def find_home_styles
    @styles = Refinery::Projects::Style.all
  end
end