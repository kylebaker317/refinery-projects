module Refinery
  module Projects
    module Admin
      class StylesController < ::Refinery::AdminController
        before_action :find_all_styles, except: [:show, :destroy]
        crudify :'refinery/projects/style',
                title_attribute: 'name'

        def edit
          @styles = Refinery::Projects::Style.all
          @style_projects = Refinery::Projects::Project.where(style: @style.id)
        end

        def new
          @styles = Refinery::Projects::Style.all
          @style = Refinery::Projects::Style.new
          @style_projects = Refinery::Projects::Project.where(style: @style.id)
        end
        protected
        def find_all_styles
          @styles = Refinery::Projects::Style.all
        end

        private

        # Only allow a trusted parameter "white list" through.
        def style_params
          params.require(:style).permit(:name, :description, :style_id, :show_in_home, :show_in_menu, :project_id)
        end
      end
    end
  end
end
