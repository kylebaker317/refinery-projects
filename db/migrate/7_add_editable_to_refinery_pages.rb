class AddEditableToRefineryPages < ActiveRecord::Migration
  def change
    add_column :refinery_pages, :editable, :boolean, default: true
  end
end
