module Refinery
  module Projects
    class Engine < Rails::Engine
      extend Refinery::Engine
      isolate_namespace Refinery::Projects

      engine_name :refinery_projects

      before_inclusion do
        Refinery::Plugin.register do |plugin|
          plugin.name = "styles"
          plugin.url = proc { Refinery::Core::Engine.routes.url_helpers.projects_admin_styles_path }
          plugin.pathname = root
          plugin.menu_match = %r{refinery/projects/styles(/.*)?$}
        end
      end

      config.after_initialize do
        Refinery.register_extension(Refinery::Styles)
      end
    end
  end
end
