class AddRefineryClientsToRefineryProjects < ActiveRecord::Migration
  def change
    add_reference :refinery_projects, :refinery_clients, index: true
    add_foreign_key :refinery_projects, :refinery_clients, column: :refinery_clients_id
  end
end
