Refinery::I18n.frontend_locales.each do |lang|
  I18n.locale = lang

  # Created Seeds
  def create_dummy_styles
    styles = %w(Traditional Transitional Contemporary Hampton Modern)
    styles.each do |style|
      Refinery::Projects::Style.create name: style
    end
  end

  def set_dummy_style_parents
    relationships = {Traditional: ['Transitional'], Modern: %w(Contemporary Hampton)}
    relationships.each do |parent_style_name, child_styles|
      parent_style = Refinery::Projects::Style.find_by name: "#{parent_style_name}"
      child_styles.each do |child_style_name|
        child_style = Refinery::Projects::Style.find_by name: "#{child_style_name}"
        child_style.update_attribute(:style_id, parent_style.id)
      end
    end
  end

  def create_dummy_projects(projects_per_client) # should be more than number of clients
    client_ids = Refinery::Clients::Client.pluck(:id)
    style_ids = Refinery::Projects::Style.pluck(:id)
    no_of_projects = client_ids.count * projects_per_client
    projects_to_add = no_of_projects - Refinery::Projects::Project.all.count
    projects_to_add.times do
      Refinery::Projects::Project.create title: Faker::Address.street_address,
                                         style_id: style_ids.sample,
                                         refinery_clients_id: client_ids.sample,
                                         description: Faker::Lorem.paragraph(5, false, 3)

    end
    if projects_to_add < 0
      puts '0 projects added.'
    else
      puts "#{projects_to_add} project(s) added."
    end

  end

  create_dummy_styles
  set_dummy_style_parents
  create_dummy_projects(8)
  puts 'Refinery::Projects dummy_data.rb ran successfully'
end
