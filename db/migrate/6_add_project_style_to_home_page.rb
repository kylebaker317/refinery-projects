class AddProjectStyleToHomePage < ActiveRecord::Migration
  def change
    change_table :refinery_projects_styles do |t|
      t.boolean :show_in_home, default: false
    end
  end
end