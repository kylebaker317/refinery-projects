
FactoryGirl.define do
  factory :style, :class => Refinery::Projects::Style do
    sequence(:name) { |n| "refinery#{n}" }
  end
end

