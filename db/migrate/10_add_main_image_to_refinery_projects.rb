class AddMainImageToRefineryProjects < ActiveRecord::Migration
  def change
    add_column :refinery_projects, :main_image_id, :integer
  end
end
