class AddShowInMenuToRefineryProjectStyles < ActiveRecord::Migration
  def change
    add_column :refinery_projects_styles, :show_in_menu, :boolean, default: false
  end
end
